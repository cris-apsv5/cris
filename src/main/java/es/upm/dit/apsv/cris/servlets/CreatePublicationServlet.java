package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = (String) request.getParameter("id");
		String title = request.getParameter("title");
		String publicationName = request.getParameter("publicationName");
		String publicationDate = request.getParameter("publicationDate");
		String authors = request.getParameter("authors");
		Publication r = new Publication();
		r.setId(id);
		r.setTitle(title);
		r.setPublicationName(publicationName);
		r.setPublicationDate(publicationDate);
		r.setAuthors(authors);
		r.setCiteCount(0);

		Client client = ClientBuilder.newClient(new ClientConfig());
		Response resp = client.target(URLHelper.getInstance().getCrisURL() + "/CRISSERVICE/rest/Publications").request()
				.post(Entity.entity(r, MediaType.APPLICATION_JSON), Response.class);

		response.sendRedirect(request.getContextPath() + "/AdminServlet");

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
