package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Publication;

@WebServlet("/UpdateCitationsAPIServlet")
public class UpdateCitationsAPIServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = (String) request.getParameter("id");
		Client client = ClientBuilder.newClient(new ClientConfig());
		Publication ri = client.target(URLHelper.getInstance().getCrisURL() + "/CRISSERVICE/rest/Publications/" + id +"/updateCiteNumber")
				.request()
				.accept(MediaType.APPLICATION_JSON).get(Publication.class);
		request.setAttribute("ri", ri);
		//getServletContext().getRequestDispatcher("/PublicationView.jsp").forward(request, response);
		response.sendRedirect(request.getContextPath() + "/PublicationServlet" + "?id=" + id);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}


